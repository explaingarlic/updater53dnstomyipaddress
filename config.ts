const confirmExists = (varName) : any => {
    if(process.env[varName]) {
        return process.env[varName]!;
    }
    else {
        console.error("You did not provide a valid value for", varName);
        process.exit(1);
    }
}

export const elIntervalino = parseInt(process.env.QUERYIPINTERVAL!) 
    ? parseInt(process.env.QUERYIPINTERVAL!) : 15 * 60 * 1000;

console.log(elIntervalino);
export const hostedZoneId = confirmExists("HOSTEDZONEID");
export const domainName = confirmExists("DOMAINNAME");