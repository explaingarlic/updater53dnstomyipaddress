import {
    elIntervalino,
    hostedZoneId,
    domainName
} from "./config";
import aws from "aws-sdk";
import axios from "axios";

const route53 = new aws.Route53();

let lastReadRecord : string | undefined = undefined;
let lastCreatedChange : string | null = null;

aws.config.update({
    region: "eu-west-1", // route53 default
});

async function changeIpTo(ipAddress : string) {
    console.log("IP was different. Changing to", ipAddress);

    if(!lastCreatedChange) {
        await route53.changeResourceRecordSets({
            HostedZoneId: hostedZoneId,
            ChangeBatch: {
                Changes: [{
                    Action: "UPSERT",
                    ResourceRecordSet: {
                        TTL: 60,
                        Name: domainName,
                        Type: "A",
                        ResourceRecords: [{
                            Value: ipAddress
                        }]
                    }
                }]
            }
        }).send((error, data)=> {
            if(error) {
                console.error("Couldn't change ip to", ipAddress, "\nError:\n\n",error);
            }

            lastCreatedChange = data.ChangeInfo.Id;
            if(process.env.NODE_ENV !== "PRODUCTION") {
                console.log(data);
            }
        });
    }
    else {
        await route53.getChange({
            Id: lastCreatedChange
        }).send((error, data)=>{
            if(error) {
                console.error("Error checking change set.\nError:\n\n", error);
            }

            if(data.ChangeInfo.Status === "PENDING") {
                // Pass.
                if(process.env.NODE_ENV !== "PRODUCTION") {
                    console.log("passing.")
                }
            }
            else {
                console.log("It is assumed that the IP has changed to", ipAddress);
                lastCreatedChange = null;
            }
        })
    }
}

async function main() {
    let resp;
    let ip;

    try {
        resp = await axios.get(`http://ip-api.com/json`);
        ip = resp.data.query;
    }
    catch(e) {
        console.error("An error occured gettin the current ip:", e);
        if(!lastReadRecord) {
            console.error("Don't try to start the app with no internet connection.");
            process.exit(1);
        }
        else {
            ip = lastReadRecord // To make it skip the following logic.
        }
    }
    // todo: error out if this query result doesn't look good.

    if(!lastReadRecord) { // first time launch of this app.
        lastReadRecord = ip;
        // TODO: decide if I want the first running of this application to try to set the IP.
        await changeIpTo(ip);
    }
    else {
        if(lastReadRecord !== ip) {
            await changeIpTo(ip);
        }
        
    };
};
main().then(()=> {
    setInterval(main, elIntervalino);
}).catch(e=>{
    console.error(e);
})

